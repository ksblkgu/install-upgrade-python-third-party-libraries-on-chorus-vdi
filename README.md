**Guide to install, maintain and upgrade python 3rd party libraries on Chorus VDI**

In this guide, it shows you how to install and maintain 3rd party python libraries on a VDI in Chorus IT environment.

*Pre-request: python version 3.5+ has been installed correctly in H drive on your VDI.*

---

### 1.	Set temporary proxy ENV variable in windows

Set both http_proxy & https_proxy variables in “System Environment Window”
Format is something like: 
~~~~
HTTP_PROXY= http://chorus\username:password@10.233.24.250:8080
HTTPS_PROXY= https://chorus\username:password@10.233.24.250:8080
~~~~

* Example & Screenshot: 

![Alt text](https://bitbucket.org/ksblkgu/install-upgrade-python-third-party-libraries-on-chorus-vdi/downloads/vdi_003.png)

---

### 2.	To install python libraries using pip

In the same CMD window, type follow commend.

~~~~
python -m pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org --trusted-host pypi.python.org python_lib_name
~~~~

*- Replace text “python_lib_name” with a valid python library name*

* Example & Screenshot:

![Alt text](https://bitbucket.org/ksblkgu/install-upgrade-python-third-party-libraries-on-chorus-vdi/downloads/vdi_002.png)

---

### 3.	To upgrade installed python libraries using pip

In the same CMD window, type follow commend.

~~~~
python -m pip install --upgrade --trusted-host pypi.org --trusted-host files.pythonhosted.org --trusted-host pypi.python.org python_lib_name
~~~~

*- Replace text “python_lib_name” with a valid python library name*

---

### 4.	To upgrade PIP version, use the following steps 

* Uninstall existing version of the PIP by running commend: *python -m uninstall pip*

* Ater uninstalled PIP, download latest version of pip install WHL file from: *https://pypi.org/project/pip/* , and place it in H:/ drive

* Run following command to install new version of PIP: 

~~~~
    python h:\pip_whl_file_name.whl/pip install --no-index h:\pip_whl_file_name.whl
~~~~

---

### 5.	Important Note: Delete both http_proxy & https_proxy variables from System Environment window after using pip commend. There will be potential ssl proxy error if both vars kept in the system envrionment.